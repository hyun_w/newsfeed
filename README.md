# News Feed 

### Description
#####1. Apis
```
1. 로그인
2. 로그아웃
3. 회원가입 
4. 페이지 생성 
5. 페이지 리스트
6. 포스트 생성 
7. 포스트 리스트 
8. 페이지 구독
9. 구독한 페이지 리스트
10. 구독 취소
11. 뉴스피드
```
#####2. Models
```
- User models
- Page models
- Post model 
- Subscription model
```

### Requirements
```bash
python >= 3.5
virtualenv
```

### Installation
```bash
$ git clone https://hyun_w@bitbucket.org/hyun_w/newsfeed.git 
 ```
 
#####install automatically
```bash
$ cd newsfeed && chmod +x run.sh
$ ./run.sh
```
#####or manually
```bash
$ echo "DEBUG = True" >> local_settings.py
$ virtualenv .venv -ppython3
$ source ./.venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py generate_dummy_data
```

### For dummy data
```bash
$ python manage.py generate_dummy_data
```
>테스트 계정
>>ID: test@test.com  
>>PASSWORD: testtest123

### For test
```bash
$ python manage.py test
```
### API Documentation
```bash
localhost:8000/docs/
```
