import coreapi
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework.decorators import api_view, renderer_classes
from rest_framework import response

schema = coreapi.Document(
    title='API',
    content={
        'signup': coreapi.Link(
            url='/signup/',
            action='post',
            fields=[
                coreapi.Field(
                    name='name',
                    required=True,
                    location='form',
                    description='user name'
                ),
                coreapi.Field(
                    name='email',
                    required=True,
                    location='form',
                    description='email'
                ),
                coreapi.Field(
                    name='password',
                    required=True,
                    location='form',
                    description='password'
                ),
            ],
            description='sign up'
        ),
        'signin': coreapi.Link(
            url='/signin/',
            action='post',
            fields=[
                coreapi.Field(
                    name='email',
                    required=True,
                    location='form',
                    description='email'
                ),
                coreapi.Field(
                    name='password',
                    required=True,
                    location='form',
                    description='password'
                ),
            ],
            description='sign up'
        ),
        'signout': coreapi.Link(
            url='/signout/',
            action='get',
            description='sign out'
        ),
        'page_list': coreapi.Link(
            url='/pages/',
            action='get',
            fields=[
                coreapi.Field(
                    name='page_size',
                    required=False,
                    location='query',
                    description='Page size. default 20'
                ),
                coreapi.Field(
                    name='cursor',
                    required=False,
                    location='query',
                    description='cursor'
                ),
            ],
            description='페이지 리스트 리턴.'
        ),
        'page_creation': coreapi.Link(
            url='/pages/',
            action='post',
            fields=[
                coreapi.Field(
                    name='name',
                    required=True,
                    location='form',
                    description='Page name'
                ),
                coreapi.Field(
                    name='description',
                    required=False,
                    location='form',
                    description='Page description'
                ),
            ],
            description='페이지 생성'
        ),
        'post_list': coreapi.Link(
            url='/pages/{page_id}/posts/',
            action='get',
            fields=[
                coreapi.Field(
                    name='page_id',
                    required=True,
                    location='path',
                    description='Page id'
                ),
                coreapi.Field(
                    name='page_size',
                    required=False,
                    location='query',
                    description='Page size. default 20'
                ),
                coreapi.Field(
                    name='cursor',
                    required=False,
                    location='query',
                    description='cursor'
                ),
            ],
            description='포스트 리스트 리턴'
        ),
        'post_creation': coreapi.Link(
            url='/pages/{page_id}/posts/',
            action='post',
            fields=[
                coreapi.Field(
                    name='page_id',
                    required=True,
                    location='path',
                    description='Page id'
                ),
                coreapi.Field(
                    name='title',
                    required=True,
                    location='form',
                    description='글 제목'
                ),
                coreapi.Field(
                    name='content',
                    required=False,
                    location='form',
                    description='글 내용'
                ),
            ],
            description='포스트 생성'
        ),
        'subscription_list': coreapi.Link(
            url='/subscriptions/',
            action='get',
            fields=[
                coreapi.Field(
                    name='page_size',
                    required=False,
                    location='query',
                    description='Page size. default 20'
                ),
                coreapi.Field(
                    name='cursor',
                    required=False,
                    location='query',
                    description='cursor'
                ),
            ],
            description='구독한 페이지 리스트'
        ),
        'subscribe': coreapi.Link(
            url='/subscriptions/',
            action='post',
            fields=[
                coreapi.Field(
                    name='page_id',
                    required=True,
                    location='form',
                    description='Page id'
                ),
            ],
            description='구독'
        ),
        'remove_subscription': coreapi.Link(
            url='/subscriptions/{subscription_id}',
            action='delete',
            fields=[
                coreapi.Field(
                    name='subscription_id',
                    required=True,
                    location='path',
                    description='subscription id'
                ),
            ],
            description='구독 취소'
        ),
        'feeds': coreapi.Link(
            url='/feeds/',
            action='get',
            fields=[
                coreapi.Field(
                    name='page_size',
                    required=False,
                    location='query',
                    description='Page size. default 20'
                ),
                coreapi.Field(
                    name='cursor',
                    required=False,
                    location='query',
                    description='cursor'
                ),
            ],
            description='뉴스 피드'
        ),
    }
)


@api_view()
@renderer_classes([SwaggerUIRenderer, OpenAPIRenderer])
def schema_view(request):
    return response.Response(schema)
