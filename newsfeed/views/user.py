from rest_framework import status
from rest_framework.response import Response
from django.contrib.auth import authenticate, login, logout

from newsfeed.models import User
from newsfeed.views.base import BaseView
from newsfeed.serializers import UserSerializer
from newsfeed.exceptions import OmittedParameterError, AuthenticationError, DeletedDataError, DuplicateDataError


class SignUpView(BaseView):
    permission_classes = ()

    def post(self, request):
        name = request.data.get('name')
        email = request.data.get('email')
        password = request.data.get('password')

        if not name:
            return OmittedParameterError('name').to_dict(), status.HTTP_400_BAD_REQUEST
        if not email:
            return OmittedParameterError('email').to_dict(), status.HTTP_400_BAD_REQUEST
        if not password:
            return OmittedParameterError('password').to_dict(), status.HTTP_400_BAD_REQUEST

        if User.objects.filter(email=email).exists():
            return DuplicateDataError().to_dict(), status.HTTP_400_BAD_REQUEST

        user = User(email=email, name=name)
        user.set_password(password)
        user.save()

        login(request, user)

        serializer = UserSerializer(instance=user)
        return serializer.data, status.HTTP_201_CREATED


class AuthenticationView(BaseView):
    permission_classes = ()

    def get(self, request):
        logout(request)
        return None, status.HTTP_200_OK

    def post(self, request):
        email = request.data.get('email')
        password = request.data.get('password')

        if not email:
            return OmittedParameterError('email').to_dict(), status.HTTP_400_BAD_REQUEST
        if not password:
            return OmittedParameterError('password').to_dict(), status.HTTP_400_BAD_REQUEST

        user = authenticate(request, email=email, password=password)
        if not user:
            return AuthenticationError().to_dict(), status.HTTP_401_UNAUTHORIZED
        if user.deleted_at:
            return DeletedDataError('User').to_dict(), status.HTTP_403_FORBIDDEN

        login(request, user)
        return Response(UserSerializer(instance=user).data)
