from rest_framework import status
from django.db.models import F
from django.core.exceptions import ValidationError
from django.db.models.functions import Coalesce, Now

from newsfeed.views.base import BaseView
from newsfeed.exceptions import InvalidFieldError, DataNotFoundError, DataFormatError, \
    DataTypeError, OmittedParameterError, DuplicateDataError, InvalidOperationError
from newsfeed.serializers import PageSerializer, PostSerializer, SubscriptionSerializer
from newsfeed.models import Post, Page, Subscription
from newsfeed.utils.cursor_pagination import CursorPaginator, InvalidCursor


class PageListView(BaseView):

    def get(self, request):
        pages = Page.objects.only_active().select_related('owner').all()
        count = pages.count()

        try:
            page_size = int(request.GET.get('page_size') or 20)
        except ValueError:
            return DataTypeError(request.GET.get('page_size'), 'int').to_dict(), status.HTTP_400_BAD_REQUEST

        cursor = request.GET.get('cursor') or None
        paginator = CursorPaginator(pages, ['-created_at'])
        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return e.to_dict(), status.HTTP_400_BAD_REQUEST

        serializer = PageSerializer(page, many=True)

        return {
            'items': serializer.data,
            'count': count,
            'has_next': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }

    def post(self, request):
        serializer = PageSerializer(data=request.data, context={'user': request.user})
        if serializer.is_valid():
            serializer.save()
            return serializer.data, status.HTTP_201_CREATED
        return InvalidFieldError(serializer.errors).to_dict(), status.HTTP_400_BAD_REQUEST


class PostListView(BaseView):

    def get(self, request, page_id):
        try:
            page = Page.objects.get(id=page_id, deleted_at=None)
        except Page.DoesNotExist:
            return DataNotFoundError('Page', page_id).to_dict(), status.HTTP_400_BAD_REQUEST
        except ValidationError:
            return DataFormatError(page_id, 'uuid').to_dict(), status.HTTP_400_BAD_REQUEST

        try:
            page_size = int(request.GET.get('page_size') or 20)
        except ValueError:
            return DataTypeError(request.GET.get('page_size'), 'int').to_dict(), status.HTTP_400_BAD_REQUEST

        posts = Post.objects.select_related('page__owner').filter(page=page)
        count = posts.count()

        cursor = request.GET.get('cursor') or None
        paginator = CursorPaginator(posts, ['-created_at'])

        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return e.to_dict(), status.HTTP_400_BAD_REQUEST

        serializer = PostSerializer(page, many=True)
        return {
            'items': serializer.data,
            'count': count,
            'has_next': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }

    def post(self, request, page_id):
        try:
            page = request.user.pages.get(id=page_id, deleted_at=None)
        except Page.DoesNotExist:
            return DataNotFoundError('Page', page_id).to_dict(), status.HTTP_400_BAD_REQUEST
        except ValidationError:
            return DataFormatError(page_id, 'uuid').to_dict(), status.HTTP_400_BAD_REQUEST

        serializer = PostSerializer(data=request.data, context={'page': page})
        if serializer.is_valid():
            serializer.save()
            return serializer.data, status.HTTP_201_CREATED
        return InvalidFieldError(serializer.errors) .to_dict(), status.HTTP_400_BAD_REQUEST


class SubscriptionListView(BaseView):
    def get(self, request):
        try:
            page_size = int(request.GET.get('page_size') or 20)
        except ValueError:
            return DataTypeError(request.GET.get('page_size'), 'int').to_dict(), status.HTTP_400_BAD_REQUEST

        subscriptions = request.user.subscriptions.select_related('user', 'page').filter(deleted_at=None).all()
        count = subscriptions.count()

        cursor = request.GET.get('cursor') or None
        paginator = CursorPaginator(subscriptions, ['-created_at'])

        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return e.to_dict(), status.HTTP_400_BAD_REQUEST

        serializer = SubscriptionSerializer(page, many=True)
        return {
            'items': serializer.data,
            'count': count,
            'has_next': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }

    def post(self, request):
        page_id = request.data.get('page_id')
        if not page_id:
            return OmittedParameterError('page_id').to_dict(), 400

        try:
            page = Page.objects.only_active().get(id=page_id)
        except Page.DoesNotExist:
            return DataNotFoundError('Page', page_id).to_dict(), status.HTTP_400_BAD_REQUEST
        except ValidationError:
            return DataFormatError(page_id, 'uuid').to_dict(), status.HTTP_400_BAD_REQUEST

        if Subscription.objects.filter(user=request.user, page=page).exists():
            return DuplicateDataError().to_dict(), status.HTTP_400_BAD_REQUEST

        subscription = Subscription.objects.create(user=request.user, page=page)
        serializer = SubscriptionSerializer(subscription)
        return serializer.data, 201


class SubscribeDeletionView(BaseView):
    def delete(self, request, subscription_id):
        try:
            subscription = request.user.subscriptions.select_related(
                'page__owner'
            ).get(
                id=subscription_id,
                deleted_at=None
            )
        except Subscription.DoesNotExist:
            return DataNotFoundError('subscription', subscription_id).to_dict(), status.HTTP_400_BAD_REQUEST
        except ValidationError:
            return DataFormatError(subscription_id, 'uuid').to_dict(), status.HTTP_400_BAD_REQUEST

        if subscription.page.owner == request.user:
            return InvalidOperationError('Can not remove my page subscription').to_dict(), status.HTTP_400_BAD_REQUEST
        subscription.delete()
        return None, 200


class NewsFeedView(BaseView):
    def get(self, request):
        try:
            page_size = int(request.GET.get('page_size') or 20)
        except ValueError:
            return DataTypeError(request.GET.get('page_size'), 'int').to_dict(), status.HTTP_400_BAD_REQUEST

        posts = Post.objects.select_related('page__owner').only_active().filter(
            page__isnull=False,
            page__subscriptions__isnull=False,
            page__subscriptions__user=request.user,
            created_at__gte=F('page__subscriptions__created_at'),
            created_at__lt=Coalesce('page__subscriptions__deleted_at', Now())
        )
        count = posts.count()

        cursor = request.GET.get('cursor') or None
        paginator = CursorPaginator(posts, ['-created_at'])

        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return e.to_dict(), status.HTTP_400_BAD_REQUEST

        serializer = PostSerializer(page, many=True)
        return {
            'items': serializer.data,
            'count': count,
            'has_next': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }
