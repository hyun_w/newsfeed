from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class BaseView(APIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers

        try:
            self.initial(request, *args, **kwargs)

            if request.method.lower() in self.http_method_names:
                handler = getattr(self, request.method.lower(),
                                  self.http_method_not_allowed)
            else:
                handler = self.http_method_not_allowed

            response = handler(request, *args, **kwargs)

        except Exception as exc:
            response = self.handle_exception(exc)

        if isinstance(response, (HttpResponse, Response)):
            response = response
        elif isinstance(response, tuple):
            key = 'error' if response[1] >= 400 else 'data'
            response = Response({key: response[0]}, status=response[1])
        elif isinstance(response, (dict, list)):
            response = Response({'data': response})
        else:
            raise TypeError

        self.response = self.finalize_response(request, response, *args, **kwargs)
        return self.response
