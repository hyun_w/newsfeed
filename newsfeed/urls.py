from django.urls import path
from newsfeed.views.user import AuthenticationView, SignUpView
from newsfeed.views.post import PageListView, PostListView, SubscriptionListView, SubscribeDeletionView, NewsFeedView


urlpatterns = [
    path('signin/', AuthenticationView.as_view(), name='signin'),
    path('signout/', AuthenticationView.as_view(), name='signin'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('pages/', PageListView.as_view(), name='pages'),
    path('pages/<str:page_id>/posts/', PostListView.as_view(), name='posts'),
    path('subscriptions/', SubscriptionListView.as_view(), name='subscriptions'),
    path('subscriptions/<str:subscription_id>/', SubscribeDeletionView.as_view(), name='subscription'),
    path('feeds/', NewsFeedView.as_view(), name='feeds')
]
