import re


class BaseError(Exception):
    description = ''

    def __init__(self, *args):
        self.args = args

    @property
    def slug(self):
        str1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', self.__class__.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', str1).upper()

    @property
    def detail_message(self):
        return self.description.format(*self.args)

    def to_dict(self):
        return {'error': self.slug, 'description': self.detail_message}


class InvalidFieldError(BaseError):
    description = 'One or more fields are invalid'

    def __init__(self, errors: dict):
        super().__init__()
        self.errors = errors

    def to_dict(self):
        return {'fields': self.errors, **super().to_dict()}


class OmittedParameterError(BaseError):
    description = '{} is required parameter'


class DataNotFoundError(BaseError):
    description = '{} is not found with {}'


class DataFormatError(BaseError):
    description = '{} is not {} format'


class DataTypeError(BaseError):
    description = '{} is not {} type'


class DuplicateDataError(BaseError):
    description = 'Data already exists'


class AuthenticationError(BaseError):
    description = 'Sign in failed'


class DeletedDataError(BaseError):
    description = '{} is deleted'


class InvalidOperationError(BaseError):
    description = '{}'
