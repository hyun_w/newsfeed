from rest_framework import serializers

from newsfeed.models import Post, Page, Subscription
from newsfeed.serializers import UserSerializer


class PageSerializer(serializers.ModelSerializer):
    owner = UserSerializer(required=False)

    class Meta:
        model = Page
        fields = ('id', 'name', 'owner', 'description', 'created_at')

    def create(self, validated_data):
        user = self.context['user']
        page = Page(**validated_data)
        page.owner = user
        page.save()

        Subscription.objects.create(user=user, page=page)
        return page


class PostSerializer(serializers.ModelSerializer):
    page = PageSerializer(required=False)

    class Meta:
        model = Post
        fields = ('id', 'title', 'content', 'page', 'created_at')

    def create(self, validated_data):
        page = self.context['page']
        post = Post(**validated_data)
        post.page = page
        post.save()

        return post


class SubscriptionSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)
    page = PageSerializer(required=False)

    class Meta:
        model = Subscription
        fields = ('id', 'user', 'page', 'created_at', 'deleted_at')

    def create(self, validated_data):
        page = self.context['page']
        user = self.context['user']

        subscription = Subscription(**validated_data)
        subscription.user = user
        subscription.page = page
        subscription.save()

        return subscription
