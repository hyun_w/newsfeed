import datetime

from django.core.management.base import BaseCommand
from newsfeed.models import User, Post, Page, Subscription


class Command(BaseCommand):
    def handle(self, *args, **options):
        pages = []
        for _ in range(3):
            user = self.create_user(email=f'test{_}@test.com')
            page = self.create_page(user=user)
            self.create_posts(page=page, count=30)

            pages.append(page)

        user = self.create_user('test@test.com')
        for page in pages:
            subscription = self.subscribe(page=page, user=user)
            Subscription.objects.filter(
                id=subscription.id
            ).update(
                created_at=datetime.datetime.now() - datetime.timedelta(days=1)
            )

    def create_user(self, email):
        return User.objects.create_user(
            email=email,
            password='testtest123',
            name='테스트유저',
            phone_number='01000000000',
        )

    def create_page(self, user):
        return Page.objects.create(owner=user, name=f'테스트페이지_{user.email}')

    def create_posts(self, page, count=1):
        posts = [Post(title=f'테스트 포스트{_}', content='테스트', page=page) for _ in range(count)]
        Post.objects.bulk_create(posts)
        return posts

    def subscribe(self, page, user):
        return Subscription.objects.create(user=user, page=page)
