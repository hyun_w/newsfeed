from django.urls import reverse
from rest_framework.test import APITestCase
from newsfeed.models import User


class UserTest(APITestCase):
    def test_create_user(self):
        data = {
            'email': 'test@test.com',
            'password': 'testtest123',
            'name': '테스트계정',
        }

        url = reverse('signup')
        res = self.client.post(url, data, format='json')

        self.assertEqual(res.status_code, 201)
        self.assertEqual(User.objects.count(), 1)
        res = res.json()['data']
        self.assertEqual(res['email'], data['email'])
        self.assertEqual(res['name'], data['name'])

    def test_sign_in(self):
        user = User(email='test@test.com', name='테스트계정')
        user.set_password('testtest123')
        user.save()

        data = {'email': 'test@test.com', 'password': 'testtest123'}

        url = reverse('signin')
        res = self.client.post(url, data, format='json')

        self.assertEqual(res.status_code, 200)
        res = res.json()
        self.assertEqual(res['email'], user.email)
        self.assertEqual(res['name'], user.name)

    def test_sign_in_authenticate_fail(self):
        user = User(email='test@test.com', name='테스트계정')
        user.set_password('testtest123')
        user.save()

        data = {'email': 'test@test.com', 'password': 'abcdefg123'}

        url = reverse('signin')
        res = self.client.post(url, data, format='json')
        self.assertEqual(res.status_code, 401)
        res = res.json()['error']
        self.assertEqual(res['error'], 'AUTHENTICATION_ERROR')

    def test_sign_in_deleted_user_fail(self):
        user = User(email='test@test.com', name='테스트계정')
        user.set_password('testtest123')
        user.save()
        user.delete()

        data = {'email': 'test@test.com', 'password': 'testtest123'}

        url = reverse('signin')
        res = self.client.post(url, data, format='json')
        self.assertEqual(res.status_code, 403)
        res = res.json()['error']
        self.assertEqual(res['error'], 'DELETED_DATA_ERROR')


