import datetime
from typing import Iterable

from django.urls import reverse
from rest_framework.test import APITestCase

from newsfeed.models import User, Page, Subscription, Post


class NewsFeedTest(APITestCase):
    def setUp(self):
        users = []
        for i in range(0, 3):
            user = User(email=f'test{i}@test.com', name=f'테스트계정{i}')
            user.set_password('testtest123')
            user.save()
            users.append(user)

        self.users = users
        self.user = users[0]
        self.client.force_login(self.user)

        self.pages = [Page(owner=user, name=f'test page{idx}') for idx, user in enumerate(users)]
        Page.objects.bulk_create(self.pages)

    def create_posts(self, page: Page, date_range: Iterable['datetime.datetime']):
        """
            Create posts with given datetime range.
            Each date has a post.
        :param page: Page
        :param date_range: Iterable with two items: start, end. each item must be datetime type.
        :return: [Post]
        """

        delta = date_range[1] - date_range[0]
        posts = [Post(page=page, title=f'test{d}', content='test content') for d in range(delta.days)]
        Post.objects.bulk_create(posts)
        for post, d in zip(posts, range(delta.days)):
            Post.objects.filter(id=post.id).update(created_at=date_range[0] + datetime.timedelta(days=d))

    def subscribe(self, page: Page, user: User, subscribed_at: datetime.datetime):
        subscription = Subscription.objects.create(page=page, user=user)
        Subscription.objects.filter(id=subscription.id).update(created_at=subscribed_at)
        subscription.refresh_from_db()
        return subscription

    def test_empty_feed(self):
        url = reverse('feeds')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

        expected_result = {
            'data': {
                'items': [],
                'has_next': False,
                'cursor': None,
                'count': 0
            }
        }
        self.assertDictEqual(res.json(), expected_result)

    def test_single_page_subscription_feed_success(self):
        start_at = datetime.datetime(2019, 4, 1)
        end_at = datetime.datetime(2019, 5, 1)
        page = self.pages[1]
        self.create_posts(page=page, date_range=(start_at, end_at))
        self.subscribe(page=page, user=self.user, subscribed_at=start_at + datetime.timedelta(days=1))

        url = reverse('feeds')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

        res = res.json()['data']
        self.assertEqual(len(res['items']), 20)
        self.assertTrue(res['has_next'])

        res = self.client.get(url, {'cursor': res['cursor']})

        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 9)
        self.assertFalse(res['has_next'])

        self.assertEqual(res['items'][-1]['created_at'], '2019-04-02 00:00:00')

    def test_removed_subscription_feed(self):
        start_at = datetime.datetime(2019, 4, 1)
        end_at = datetime.datetime(2019, 5, 1)
        page = self.pages[1]
        self.create_posts(page=page, date_range=(start_at, end_at))
        subscription = self.subscribe(page=page, user=self.user, subscribed_at=start_at)

        subscription.deleted_at = datetime.datetime(2019, 4, 20)
        subscription.save()

        url = reverse('feeds')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

        res = res.json()['data']
        self.assertEqual(len(res['items']), 19)
        self.assertEqual(res['items'][0]['created_at'], '2019-04-19 00:00:00')
        self.assertFalse(res['has_next'])

    def test_multiple_page_feed_success(self):
        self.create_posts(page=self.pages[1], date_range=(datetime.datetime(2019, 4, 1), datetime.datetime(2019, 5, 1)))
        self.create_posts(page=self.pages[2], date_range=(datetime.datetime(2019, 3, 1), datetime.datetime(2019, 5, 1)))

        self.subscribe(page=self.pages[1], user=self.user, subscribed_at=datetime.datetime(2019, 4, 1))
        self.subscribe(page=self.pages[2], user=self.user, subscribed_at=datetime.datetime(2019, 3, 1))

        url = reverse('feeds')
        cursor = None
        while True:
            res = self.client.get(url, {'cursor': cursor} if cursor else {})
            self.assertEqual(res.status_code, 200)

            res = res.json()['data']
            if not res['has_next']:
                self.assertEqual(len(res['items']), 11)
                self.assertEqual(res['items'][-1]['created_at'], '2019-03-01 00:00:00')
                break
            self.assertEqual(len(res['items']), 20)
            cursor = res['cursor']

    def test_list_invalid_page_size_fail(self):
        url = reverse('feeds')
        res = self.client.get(url, {'page_size': 'string'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DATA_TYPE_ERROR')

    def test_list_invalid_cursor_fail(self):
        url = reverse('feeds')
        res = self.client.get(url, {'cursor': 'wrongCursor'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'INVALID_CURSOR')
