import uuid
from django.urls import reverse
from rest_framework.test import APITestCase
from newsfeed.models import User, Page, Subscription


class SubscribeTest(APITestCase):
    def setUp(self):
        users = []
        for i in range(0, 2):
            user = User(email=f'test{i}@test.com', name=f'테스트계정{i}')
            user.set_password('testtest123')
            user.save()
            users.append(user)

        self.users = users
        self.user = users[0]
        self.client.force_login(self.user)
        self.page = Page.objects.create(owner=users[1], name='test page')

    def test_soft_deletion(self):
        subscription = Subscription.objects.create(page=self.page, user=self.user)
        subscription.delete()
        self.assertIsNotNone(Subscription.objects.first().deleted_at)

        subscription.deleted_at = None
        subscription.save()
        Subscription.objects.filter(id=subscription.id).delete()
        self.assertIsNotNone(Subscription.objects.first().deleted_at)

    def test_list_subscription(self):
        url = reverse('subscriptions')

        owner = User.objects.exclude(id=self.user.id).first()
        pages = [Page(owner=owner, name=f'name{i}') for i in range(0, 30)]
        Page.objects.bulk_create(pages)
        Subscription.objects.bulk_create([Subscription(page=page, user=self.user) for page in pages])

        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 20)
        self.assertEqual(res['has_next'], True)

        res = self.client.get(url, {'cursor': res['cursor']})
        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 10)
        self.assertEqual(res['has_next'], False)

    def test_empty_list_subscription(self):
        url = reverse('subscriptions')

        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 0)
        self.assertEqual(res['has_next'], False)

    def test_list_invalid_cursor(self):
        url = reverse('subscriptions')

        res = self.client.get(url, {'cursor': 'wrongCursor'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'INVALID_CURSOR')

    def test_subscribe(self):
        url = reverse('subscriptions')

        res = self.client.post(url, {'page_id': self.page.id})
        self.assertEqual(res.status_code, 201)
        self.assertEqual(Subscription.objects.filter(user=self.user).count(), 1)

    def test_subscribe_no_page_fail(self):
        url = reverse('subscriptions')

        res = self.client.post(url, {'page_id': uuid.uuid4()})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DATA_NOT_FOUND_ERROR')

    def test_subscribe_duplicate_fail(self):
        Subscription.objects.create(page=self.page, user=self.user)
        url = reverse('subscriptions')

        res = self.client.post(url, {'page_id': self.page.id})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DUPLICATE_DATA_ERROR')

    def test_remove_subscription(self):
        subscription = Subscription.objects.create(page=self.page, user=self.user)
        url = f'/subscriptions/{subscription.id}/'
        res = self.client.delete(url)
        self.assertEqual(res.status_code, 200)
        res = res.json()
        self.assertIsNone(res['data'])
        self.assertIsNotNone(Subscription.objects.get(id=subscription.id).deleted_at)

    def test_remove_my_subscription_fail(self):
        user = self.users[1]
        self.client.force_login(user)
        subscription = Subscription.objects.create(page=self.page, user=user)
        url = f'/subscriptions/{subscription.id}/'
        res = self.client.delete(url)
        self.assertEqual(res.status_code, 400)

    def test_remove_not_my_subscription_fail(self):
        subscription = Subscription.objects.create(page=self.page, user=self.users[1])
        url = f'/subscriptions/{subscription.id}/'
        res = self.client.delete(url)
        self.assertEqual(res.status_code, 400)
