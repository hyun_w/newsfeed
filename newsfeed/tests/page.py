from django.urls import reverse
from rest_framework.test import APITestCase
from newsfeed.models import User, Page, Subscription


class PageTest(APITestCase):
    def setUp(self):
        user = User(email='test@test.com', name='테스트계정')
        user.set_password('testtest123')
        user.save()

        self.user = user
        self.client.force_login(user)

    def test_soft_deletion(self):
        page = Page.objects.create(owner=self.user, name='test')
        page.delete()
        self.assertIsNotNone(Page.objects.first().deleted_at)

        page.deleted_at = None
        page.save()
        Page.objects.filter(id=page.id).delete()
        self.assertIsNotNone(Page.objects.first().deleted_at)

    def test_page_list(self):
        url = reverse('pages')

        Page.objects.bulk_create([Page(owner=self.user, name=f'test page {i}') for i in range(0, 30)])
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 20)
        self.assertEqual(res['has_next'], True)

        res = self.client.get(url, {'cursor': res['cursor']})
        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 10)
        self.assertEqual(res['has_next'], False)

    def test_list_invalid_page_size_fail(self):
        url = reverse('pages')
        res = self.client.get(url, {'page_size': 'string'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DATA_TYPE_ERROR')

    def test_list_invalid_cursor_fail(self):
        url = reverse('pages')
        res = self.client.get(url, {'cursor': 'wrongCursor'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'INVALID_CURSOR')

    def test_create_page(self):
        url = reverse('pages')

        data = {'name': 'test', 'description': 'test'}

        res = self.client.post(url, data, format='json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(Page.objects.count(), 1)
        self.assertEqual(Subscription.objects.filter(user=self.user).count(), 1)

    def test_create_duplicated_name_fail(self):
        url = reverse('pages')
        Page.objects.create(name='test', description='test', owner=self.user)

        data = {'name': 'test', 'description': 'test'}
        res = self.client.post(url, data, format='json')
        self.assertEqual(res.status_code, 400)


