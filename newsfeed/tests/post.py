import uuid
from rest_framework.test import APITestCase
from newsfeed.models import User, Page, Post


class PostTest(APITestCase):
    def setUp(self):
        users = []
        for i in range(0, 2):
            user = User(email=f'test{i}@test.com', name=f'테스트계정{i}')
            user.set_password('testtest123')
            user.save()
            users.append(user)

        self.users = users
        self.user = users[0]
        self.client.force_login(self.user)
        self.page = Page.objects.create(owner=users[1], name='test page')
        self.list_url = f'/pages/{self.page.id}/posts/'

    def test_soft_deletion(self):
        post = Post.objects.create(page=self.page, title='test', content='test')
        post.delete()
        self.assertIsNotNone(Post.objects.first().deleted_at)

        post.deleted_at = None
        post.save()
        Post.objects.filter(id=post.id).delete()
        self.assertIsNotNone(Post.objects.first().deleted_at)

    def test_post_creation(self):
        data = {'title': 'test title', 'content': 'test content'}

        self.client.force_login(self.users[1])
        res = self.client.post(self.list_url, data, format='json')
        self.assertEqual(res.status_code, 201)

        res = res.json()
        posts = Post.objects.all()
        self.assertEqual(posts.count(), 1)
        self.assertEqual(res['data']['id'], str(posts.first().id))

    def test_creation_invalid_parameter_fail(self):
        data = {'content': 'test content'}

        self.client.force_login(self.users[1])
        res = self.client.post(self.list_url, data, format='json')
        self.assertEqual(res.status_code, 400)

    def test_creation_user_permission_fail(self):
        data = {'content': 'test content'}

        res = self.client.post(self.list_url, data, format='json')
        self.assertEqual(res.status_code, 400)

    def test_list(self):
        Post.objects.bulk_create([Post(page=self.page, title=f'title{i}', content=f'{i}') for i in range(0, 30)])
        res = self.client.get(self.list_url)
        self.assertEqual(res.status_code, 200)

        res = res.json()['data']
        self.assertEqual(len(res['items']), 20)
        self.assertEqual(res['has_next'], True)

        res = self.client.get(self.list_url, {'cursor': res['cursor']})

        self.assertEqual(res.status_code, 200)
        res = res.json()['data']
        self.assertEqual(len(res['items']), 10)
        self.assertEqual(res['has_next'], False)

    def test_list_invalid_page_id_fail(self):
        res = self.client.get(f'/pages/wrong_format/posts/')
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DATA_FORMAT_ERROR')

    def test_list_no_page_fail(self):
        res = self.client.get(f'/pages/{uuid.uuid4()}/posts/')
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DATA_NOT_FOUND_ERROR')

    def test_list_invalid_page_size_fail(self):
        res = self.client.get(self.list_url, {'page_size': 'string'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'DATA_TYPE_ERROR')

    def test_list_invalid_cursor_fail(self):
        res = self.client.get(self.list_url, {'cursor': 'wrongCursor'})
        self.assertEqual(res.status_code, 400)
        res = res.json()
        self.assertEqual(res['error']['error'], 'INVALID_CURSOR')


