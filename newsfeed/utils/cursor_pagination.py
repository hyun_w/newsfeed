from base64 import b64decode, b64encode
from collections import Sequence

from django.utils import six
from django.db.models import Q


class InvalidCursor(Exception):
    description = '{} is invalid cursor'

    def __init__(self, cursor):
        self.cursor = cursor

    def to_dict(self):
        return {'error': 'INVALID_CURSOR', 'description': self.description.format(self.cursor)}


class CursorPage(Sequence):
    def __init__(self, items, has_next=False):
        self.items = items
        self.has_next = has_next

    def __len__(self):
        return len(self.items)

    def __getitem__(self, attr):
        return list(self.items).__getitem__(attr)


class CursorPaginator:
    DELIMITER = '|'

    def __init__(self, queryset, columns):
        self.queryset = queryset.order_by(*columns)
        self.ordering_columns = columns

    def page(self, page_size=None, cursor=None):
        if page_size is None:
            return CursorPage(self.queryset)

        qs = self.queryset
        if cursor:
            qs = self._filter_by_cursor(cursor, qs)

        qs = list(qs[:page_size+1])
        items = qs[:page_size]

        has_next = len(qs) > len(items)

        return CursorPage(items, has_next)

    def _filter_by_cursor(self, cursor, queryset):
        position = self._decode_cursor(cursor)
        columns = [(col.lstrip('-'), '-' in col) for col in self.ordering_columns]  # (column, reversed[boolean])

        prv = None

        for col, p in zip(columns[::-1], position[::-1]):
            key = '__lt' if col[1] is True else '__gt'
            if not prv:
                prv = Q(**{f'{col[0]}{key}': p})
            else:
                prv = Q(**{f'{col[0]}{key}e': p}) & (Q(**{f'{col[0]}{key}': p}) | prv)
        return queryset.filter(prv)

    def _encode_cursor(self, position):
        return b64encode(self.DELIMITER.join(position).encode('ascii')).decode('ascii')

    def _decode_cursor(self, cursor):
        try:
            decoded_cursor = b64decode(cursor.encode('ascii')).decode('ascii')
            return decoded_cursor.split(self.DELIMITER)

        except (TypeError, ValueError):
            raise InvalidCursor(cursor)

    def _get_position(self, instance):
        position = []

        for column in self.ordering_columns:
            parts = column.lstrip('-').split('__')
            attr = instance

            while parts:
                attr = getattr(attr, parts[0])
                parts.pop(0)
            position.append(six.text_type(attr))

        return position

    def cursor(self, instance):
        position = self._get_position(instance)
        return self._encode_cursor(position)
