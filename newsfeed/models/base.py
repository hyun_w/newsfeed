import datetime
from django.db import models


class SoftDeletionQuerySet(models.QuerySet):
    def only_active(self):
        return self.filter(deleted_at=None)

    def delete(self):
        return self.update(deleted_at=datetime.datetime.now())


class SoftDeletionModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(
        auto_now_add=True
    )
    deleted_at = models.DateTimeField(
        null=True
    )

    objects = SoftDeletionQuerySet.as_manager()

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = datetime.datetime.now()
        self.save()
