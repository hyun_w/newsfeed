import uuid
from newsfeed.models.base import SoftDeletionModel
from django.db import models


class Page(SoftDeletionModel):
    class Meta:
        verbose_name = '학교 페이지'
        verbose_name_plural = verbose_name
        indexes = [models.Index(fields=['-created_at'])]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(
        'User',
        on_delete=models.SET_NULL,
        related_name='pages',
        null=True
    )
    name = models.CharField(  # 명세에는 없었지만 학교당 1개의 페이지가 존재해야 하는 것으로 가정함.
        max_length=31,
        unique=True
    )
    description = models.TextField(
        blank=True
    )

    def __str__(self):
        return self.name


class Post(SoftDeletionModel):
    class Meta:
        verbose_name = '게시물'
        verbose_name_plural = verbose_name

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    page = models.ForeignKey(
        'Page',
        on_delete=models.CASCADE,
        related_name='post',
        null=True
    )
    title = models.CharField(
        max_length=100
    )
    content = models.TextField()


class Subscription(SoftDeletionModel):
    class Meta:
        verbose_name = '구독'
        verbose_name_plural = verbose_name

        indexes = [models.Index(fields=['page', '-created_at'])]
        unique_together = ('user', 'page')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(
        'User',
        on_delete=models.SET_NULL,
        related_name='subscriptions',
        null=True
    )
    page = models.ForeignKey(
        'Page',
        on_delete=models.SET_NULL,
        related_name='subscriptions',
        null=True
    )
